<?php

function tk_enqueue() {
  $version = TK_DEV_MODE === true ? time() : TK_VERSION;
  $uri = get_theme_file_uri();

  wp_register_style('tk_main', $uri . '/static/css/app.css', [], $version);
  wp_enqueue_style('tk_main');

  wp_register_script('tk_vue', $uri . '/static/js/vue.js', [], $version, true);
  // wp_register_script('tk_app', $uri . '/static/js/app.js', [], $version, true);

  wp_enqueue_script('jquery');
}