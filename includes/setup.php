<?php

function tk_setup_theme() {
  add_theme_support('post-thumbnails');
  add_theme_support('title-tag');
}