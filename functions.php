<?php

// Setup
define('TK_DEV_MODE', true);
define('TK_VERSION', 1);

// Includes
include(get_theme_file_path('/includes/front/enqueue.php'));
include(get_theme_file_path('/includes/setup.php'));

// Hooks
add_action('wp_enqueue_scripts', 'tk_enqueue');
add_action('after_setup_theme', 'tk_setup_theme');
