<?php
  get_header();
?>

<div class="container">
  <h1><?php _e( 'Page Not Found', 'default-theme' ); ?>
</div>

<?php get_footer(); ?>